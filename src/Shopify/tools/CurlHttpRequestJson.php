<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Date: 2020/8/19
 * Time: 9:06
 */

namespace Meibuyu\Micro\Shopify\tools;

use Exception;

/**
 * Curl的json格式请求
 * Class HttpRequestJson
 * @package Meibuyu\Micro\Tools
 */
class CurlHttpRequestJson
{

    protected function prepareRequest($headers, $data = [])
    {
        $data = json_encode($data);
        if (!empty($data)) {
            $headers['Content-type'] = 'application/json';
            $headers['Content-Length'] = strlen($data);
        }
        return [$headers, $data];
    }

    /**
     * @param $url
     * @param array $headers
     * @return array
     * @throws Exception
     * @author zero
     */
    public function get($url, $headers = [])
    {
        return CurlRequest::get($url, $headers);
    }

    /**
     * post请求
     * @param $url
     * @param $data
     * @param array $headers
     * @return array
     * @throws Exception
     * @author zero
     */
    public function post($url, $data, $headers = [])
    {
        [$headers, $data] = self::prepareRequest($headers, $data);
        return CurlRequest::post($url, $data, $headers);
    }

    /**
     * put请求
     * @param $url
     * @param $data
     * @param array $headers
     * @return array
     * @throws Exception
     * @author zero
     */
    public function put($url, $data, $headers = [])
    {
        [$headers, $data] = self::prepareRequest($headers, $data);
        return CurlRequest::put($url, $data, $headers);
    }

    /**
     * delete请求
     * @param $url
     * @param array $headers
     * @return array
     * @throws Exception
     * @author zero
     */
    public function delete($url, $headers = [])
    {
        return CurlRequest::delete($url, $headers);
    }

}
