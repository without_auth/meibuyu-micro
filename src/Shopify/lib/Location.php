<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Date: 2020/9/23
 * Time: 8:58
 */

namespace Meibuyu\Micro\Shopify\lib;

/**
 * Class Location
 * @package Meibuyu\Micro\Shopify\lib
 */
class Location extends AbstractShopify
{

    protected $resourceKey = 'location';

    protected $childResource = [
        'InventoryLevel',
    ];

}
