<?php

namespace Meibuyu\Micro\Tools;
use Dimsav\UnixZipper;
use Meibuyu\Micro\Exceptions\HttpResponseException;

/**
 * 打包器
 * Class PackFiler
 */
class Unpacker
{
    //文件列表 一位数组
    public $fileList;
    //文件名
    public $fileName;

    public function __construct(array $fileList, $fileName)
    {
        $this->fileList = $fileList;
        $this->fileName = $fileName;
    }

    /**
     * 打包
     * @return string
     * @throws \Exception
     */
    public function unpack()
    {
        if (count($this->fileList) > 0) {
            $zipper = new UnixZipper();
            $filePath = $this->fileName.'.zip';
            foreach ($this->fileList as $k => $v) {
                $zipper->add($v);
            }
            $zipper->setDestination($filePath);
            $zipper->compress();
            return $filePath;
        } else {
            throw new HttpResponseException('打包失败，请重试');
        }

    }
}