<?php
/**
 * Created by PhpStorm.
 * User: 王源
 * Date: 2020/1/9
 * Time: 15:07
 */

namespace Meibuyu\Micro\Service\Interfaces\Product;

interface PlatformProductServiceInterface
{

    /**
     * 获取单个数据
     * @param int $id 平台产品id
     * @param array $relations 平台产品的关联关系，
     * 支持：["status","product","amazon_warehouse","platform_product_children","brand","category","ingredient","product_name","images","price_info","property"]
     * @param array $columns 平台产品表的字段，默认全部字段
     * ['id', 'sku', 'product_id', 'name', 'team_id', 'site_id', 'price', 'currency_id', 'platform_product_status_id', 'creator_id', 'asin', 'amazon_warehouse_id', 'info_completed']
     * @return array|null
     */
    public function get($id, array $relations = [], $columns = ['*']);

    /**
     * 通过id列表获取平台产品数组
     * @param array $idList 平台产品id的列表, 默认去重
     * @param array $relations 平台产品的关联关系，
     * 支持：["status","product","amazon_warehouse","platform_product_children","brand","category","ingredient","product_name","images","price_info","property"]
     * @param array $columns 平台产品表的字段，默认全部字段
     * ['id', 'sku', 'product_id', 'name', 'team_id', 'site_id', 'price', 'currency_id', 'platform_product_status_id', 'creator_id', 'asin', 'amazon_warehouse_id', 'info_completed']
     * @return array 默认keyBy('id')
     */
    public function getByIdList(array $idList, array $relations = [], $columns = ['*']): array;

    /**
     * 获取申报要素数据
     * @param array $idList
     * @param bool $groupByFlag
     * @return array
     */
    public function getWithPoint(array $idList, $groupByFlag = false);

    /**
     * 获取全部亚马逊仓库
     * @return array
     */
    public function amazonWarehouses();

}
