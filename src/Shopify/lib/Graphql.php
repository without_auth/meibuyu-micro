<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Date: 2021/01/08
 * Time: 09:34:30
 */

namespace Meibuyu\Micro\Shopify\lib;

use Exception;
use Meibuyu\Micro\Shopify\tools\HttpRequestGraphQL;

/**
 * Class Graphql
 * @package Meibuyu\Micro\Shopify\lib
 */
class Graphql extends AbstractShopify
{

    protected $resourceKey = 'graphql';

    protected $pluralizeKey = 'graphql';

    public function post($graphQL, $url = null, $wrapData = false, $variables = null)
    {
        if (!$url) $url = $this->generateUrl();
        $response = HttpRequestGraphQL::post($url, $graphQL, $this->httpHeaders, $variables);
        return $this->processResponse($response);
    }

    public function get($urlParams = array(), $url = null, $dataKey = null)
    {
        throw new Exception("GraphQL 只支持 POST 请求!");
    }

    public function put($id, $dataArray, $url = null, $wrapData = true)
    {
        throw new Exception("GraphQL 只支持 POST 请求!");
    }

    public function delete($id = null, $urlParams = [], $url = null)
    {
        throw new Exception("GraphQL 只支持 POST 请求!");
    }

}
