<?php

namespace Meibuyu\Micro\Tools;

use Psr\SimpleCache\CacheInterface;

/**
 * Class Cache
 * @package Meibuyu\Micro\Tools
 * @method static get($key, $default = null)
 * @method static set($key, $value, $ttl = null)
 * @method static delete($key)
 * @method static clear()
 * @method static getMultiple($keys, $default = null)
 * @method static setMultiple($values, $ttl = null)
 * @method static deleteMultiple($keys)
 * @method static has($key)
 */
class Cacher
{

    /**
     * @var CacheInterface
     */
    protected static $driver;

    /**
     * @return CacheInterface
     */
    public static function driver()
    {
        if (!self::$driver) {
            self::$driver = container(CacheInterface::class);
        }
        return self::$driver;
    }

    public static function __callStatic($name, $arguments)
    {
        return static::driver()->{$name}(...$arguments);
    }

}