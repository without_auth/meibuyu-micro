<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Date: 2020/9/17
 * Time: 8:40
 */

namespace Meibuyu\Micro\Tools;

use Exception;

/**
 * 图片工具类
 * Class Imager
 * @package Meibuyu\Micro\Tools
 */
class Imager
{

    /**
     * 图片转base64,暂不支持外部地址图片
     * @param $path
     * @param bool $noPrefix 不需要前缀
     * @return string
     * @throws Exception
     */
    public static function img2Base64($path, $noPrefix = false)
    {
        $path = self::parsePath($path);
        if (file_exists($path)) {
            $fp = fopen($path, "r"); // 图片是否可读权限
            if ($fp) {
                $imgInfo = getimagesize($path); // 取得图片的大小，类型等
                $content = fread($fp, filesize($path));
                $content = chunk_split(base64_encode($content)); // base64编码
                if ($noPrefix) {
                    $base64 = $content;
                } else {
                    $base64 = 'data:' . $imgInfo['mime'] . ';base64,' . $content; // 合成图片的base64编码
                }
                fclose($fp);
                return $base64;
            } else {
                throw new Exception('图片读取失败');
            }
        } else {
            throw new Exception('图片不存在');
        }
    }

    /**
     * @param $path
     * @return string
     * @throws Exception
     */
    public static function parsePath($path)
    {
        if (strstr($path, 'http')) {
            $appDomain = config('app_domain');
            if (strstr($path, $appDomain)) {
                $path = str_replace($appDomain, '', $path);
            } else {
                throw new Exception('暂不支持外部地址图片');
            }
        }
        $documentRoot = config('server.settings.document_root');
        if (!strstr($path, $documentRoot)) {
            $path = trim(trim($path), '/');
            $path = $documentRoot . '/' . $path;
        }
        return $path;
    }

}
