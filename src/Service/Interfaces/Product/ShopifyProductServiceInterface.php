<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Date: 2020/10/12
 * Time: 9:39
 */

namespace Meibuyu\Micro\Service\Interfaces\Product;

interface ShopifyProductServiceInterface
{

    /**
     * 通过shopify子产品的shopify_id获取shopify子产品
     * @param array $vids 默认去重
     * @return array 默认keyBy
     */
    public function getChildrenByVids(array $vids): array;

}
