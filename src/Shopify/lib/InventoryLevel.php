<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Date: 2020/9/23
 * Time: 8:58
 */

namespace Meibuyu\Micro\Shopify\lib;

/**
 * Class InventoryLevel
 * @package Meibuyu\Micro\Shopify\lib
 *
 * @method array adjust($data)  Adjust inventory level.
 * @method array connect($data) Connect an inventory item to a location.
 * @method array set($data)     Sets an inventory level for a single inventory item within a location.
 */
class InventoryLevel extends AbstractShopify
{

    protected $resourceKey = 'inventory_level';

    protected $customPostActions = [
        'adjust',
        'connect',
        'set',
    ];

}
