<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Date: 2020/8/24
 * Time: 16:50
 */

namespace Meibuyu\Micro\Shopify\lib;

/**
 * Class Collection
 * @package Meibuyu\Micro\Shopify\lib
 *
 * @property-read Metafield $Metafield
 *
 * @method Metafield Metafield(integer $id = null)
 */
class Collection extends AbstractShopify
{

    protected $resourceKey = 'collection';

    protected $childResource = [
        'Metafield',
    ];

}
