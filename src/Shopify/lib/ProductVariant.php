<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Date: 2020/9/2
 * Time: 16:50
 */

namespace Meibuyu\Micro\Shopify\lib;

/**
 * Class ProductVariant
 * @package Meibuyu\Micro\Shopify\lib
 *
 * @property-read Metafield $Metafield
 *
 * @method Metafield Metafield(integer $id = null)
 */
class ProductVariant extends AbstractShopify
{

    protected $resourceKey = 'variant';

    protected $childResource = [
        'Metafield',
    ];

}
