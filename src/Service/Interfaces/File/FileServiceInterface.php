<?php


namespace Meibuyu\Micro\Service\Interfaces\File;


interface FileServiceInterface
{
    /**
     * @param string $type 类型编码 0001
     * @param integer $number 生成条形码数量 100
     * @return mixed
     */
    public function generateBarCode($type, $number);
}