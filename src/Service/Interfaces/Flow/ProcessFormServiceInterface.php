<?php


namespace Meibuyu\Micro\Service\Interfaces\Flow;


interface ProcessFormServiceInterface
{
    /**
     * 创建流程申请
     * @param integer $processId 流程id
     * @param string $url 表单图片地址
     * @param array $data 表单详细内容
     * @param string $user 当前用户信息
     * @return mixed
     */
    public function createProcessForm($processId, $url, array $data, $user);

    /**
     * @param array $ApplyId 申请的id
     * @param string $operate 操作：'agree'同意  'turnDown'驳回  'plug'撤回
     * @return mixed
     */
    public function operateFlow($ApplyId, $operate);
}