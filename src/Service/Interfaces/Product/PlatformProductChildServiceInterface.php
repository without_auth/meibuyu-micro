<?php
/**
 * Created by PhpStorm.
 * User: 王源
 * Date: 2020/1/9
 * Time: 15:07
 */

namespace Meibuyu\Micro\Service\Interfaces\Product;

interface PlatformProductChildServiceInterface
{

    /**
     * 获取单个数据
     * @param int $id 平台产品id
     * @param array $relations 平台子产品的关联关系，支持：["platform_product","product_child","weight"]
     * @param array $columns 平台子产品表的字段，默认全部字段
     * ['id','platform_product_id','product_child_id','platform_product_child_sku','asin','fnsku','is_sale']
     * @return array|null
     */
    public function get($id, array $relations = [], $columns = ['*']);

    /**
     * 通过id列表获取平台子产品数组
     * @param array $idList 平台子产品id的列表, 默认去重
     * @param array $relations 平台子产品的关联关系，支持：["platform_product","product_child","weight","packs"]
     * @param array $columns 平台子产品表的字段，默认全部字段
     * ['id','platform_product_id','product_child_id','platform_product_child_sku','asin','fnsku','is_sale']
     * @return array 默认keyBy('id')
     */
    public function getByIdList(array $idList, array $relations = [], $columns = ['*']): array;

    /**
     * 获取单个数据
     * @param string $sku 平台子产品sku
     * @param int $siteId 平台子产品所属站点
     * @param array $relations 平台子产品的关联关系，支持：["platform_product","product_child","weight","packs"]
     * @param array $columns 平台子产品表的字段，默认全部字段
     * ['id','platform_product_id','product_child_id','platform_product_child_sku','asin','fnsku','is_sale']
     * @return array
     */
    public function getBySku($sku, $siteId, array $relations = [], $columns = ['*']): array;

    /**
     * 通过sku列表获取平台子产品数组
     * @param array $skuList 平台子产品sku的列表, 默认去重
     * @param int $siteId 平台子产品所属站点
     * @param array $relations 平台子产品的关联关系，支持：["platform_product","product_child","weight","packs"]
     * @param array $columns 平台子产品表的字段，默认全部字段
     * ['id','platform_product_id','product_child_id','platform_product_child_sku','asin','fnsku','is_sale']
     * @return array 默认keyBy('platform_product_child_sku')
     */
    public function getBySkuList(array $skuList, $siteId, array $relations = [], $columns = ['*']): array;

    /**
     * 模糊搜索平台子sku,获取id数组
     * @param string $childSku 平台子sku
     * @param array|null $limitIds 限制id数组,不传为不限制
     * @return array
     */
    public function getIdsByChildSku(string $childSku, array $limitIds = null);

    /**
     * 获取仓库主sku对应的平台主sku
     * @param array $productChildIds 已去重
     * @param null $siteId 指定站点id, 不传或传null,获取全部数据
     * @param array $columns
     * @return array
     */
    public function getListByProductChildIds(array $productChildIds, $siteId = null, array $columns = ['*']);

    /**
     * 通过仓库产品id获取平台子产品数组
     * @param $productId
     * @param null $siteId
     * @param array $relations 平台子产品的关联关系，支持：["platform_product","product_child","weight","packs"]
     * @param array $columns
     * @return array
     */
    public function getListByProductIds($productId, $siteId = null, array $relations = [], $columns = ['*']);

    /**
     * 通过颜色和尺码id获取平台子产品数组
     * @param $productId
     * @param $colorId
     * @param $sizeId
     * @param $siteId
     * @param array $relations 平台子产品的关联关系，支持：["platform_product","product_child","weight","packs"]
     * @return array
     * @author Zero
     */
    public function getListByColorSizeIds($productId, $colorId, $sizeId, $siteId, $relations = []);

    /**
     * 通过颜色和尺码id获取平台组合子产品数组
     * @param $productId
     * @param $siteId
     * @param array $colorIds
     * @param $sizeId
     * @return array
     * @author Zero
     */
    public function getPackListByColorSizeIds($productId, $siteId, $colorIds, $sizeId);

}
