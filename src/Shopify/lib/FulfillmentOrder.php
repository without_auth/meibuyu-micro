<?php

namespace Meibuyu\Micro\Shopify\lib;

/**
 * Class FulfillmentOrder
 * @package Meibuyu\Micro\Shopify\lib
 *
 * @property-read Event $Event
 *
 * @method array cancel()     Cancel a fulfillment order
 * @method array close()      Marks a fulfillment order as incomplete
 * @method array move()       Moves a fulfillment order to a new location
 */
class FulfillmentOrder extends AbstractShopify
{
    protected $resourceKey = 'fulfillment_order';

    protected $customPostActions = [
        'cancel',
        'close',
        'move',
    ];

}
