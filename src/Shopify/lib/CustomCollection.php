<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Date: 2020/8/22
 * Time: 16:14
 */

namespace Meibuyu\Micro\Shopify\lib;

/**
 * Class CustomCollection
 * @package Meibuyu\Micro\Shopify\lib
 *
 * @property-read Metafield $Metafield
 *
 * @method Metafield Metafield(integer $id = null)
 */
class CustomCollection extends AbstractShopify
{

    protected $resourceKey = 'custom_collection';

    protected $childResource = [
        'Metafield',
    ];

}
