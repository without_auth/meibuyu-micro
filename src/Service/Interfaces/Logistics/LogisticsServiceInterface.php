<?php


namespace Meibuyu\Micro\Service\Interfaces\Logistics;


interface LogisticsServiceInterface
{
    /**
     * 将物流单号推送到物流中心，物流中心会时刻监视运单变化实时推送给订阅模块
     * @param string $module 模块名称
     * @param array $logisticsInfo 物流公司信息 $logisticsInfo = [['logistics_code' => '', 'logistics_no' => '']];
     * @return mixed
     */

    public function push($module,$logisticsInfo);

    /**
     * 根据物流单号查询物流信息
     * @param array $logisticsNo 物流订单号
     * @return mixed
     */
    public function logisticsInfo($logisticsNo);
}