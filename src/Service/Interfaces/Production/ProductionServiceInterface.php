<?php

namespace Meibuyu\Micro\Service\Interfaces\Production;

use Meibuyu\Micro\Exceptions\HttpResponseException;

interface ProductionServiceInterface
{

    /**
     * 生产单批次发货入库回调
     * @param array $params
     * [
     *   'production_no' => 'SC20201218-001-001', // 生产单号,必须
     *   'auth' => Auth::user(), // 当前用户,必须
     *   'warehousing_order' => [
     *     'id' => '123', // 入库单id,必须
     *     'warehousing_no' => 'RK-20200727-001', // 入库单号,必须
     *     'count' => '123', // 入库数量, 必须
     *   ]
     * ]
     * @throws HttpResponseException
     * @author Zero
     */
    public function warehousingCallback(array $params);

    /**
     * 通过入库单id获取相关数据
     * @param array $warehousingOrderIds 默认去重
     * @return array 默认keyBy
     */
    public function getInfoForWarehousingOrder(array $warehousingOrderIds);

    /**
     * 通过生产单号获取生产单工厂信息
     * @param array $productionNoList 生产单号数组,默认去重
     * @return array 'production_no' => ['factory_id' => 1, 'factory_name' => 'AL']
     * @author Zero
     */
    public function getFactoryByProductionNoList(array $productionNoList): array;

    /**
     * 查询备货计划中是否用过仓库子产品
     * @param array $ids 仓库子产品id数组,默认去重
     * @return array 返回用过的id数组
     */
    public function searchUsedProductChildId($ids);

    /**
     * 查询备货计划中是否用过平台子产品
     * @param array $ids 平台子产品id数组,默认去重
     * @return array 返回用过的id数组
     */
    public function searchUsedPlatformProductChildId($ids);

}
