<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Date: 2020/7/3
 * Time: 14:27
 */

namespace Meibuyu\Micro\Handler;

use Hyperf\Contract\ConfigInterface;
use Hyperf\Di\Annotation\Inject;
use Meibuyu\Micro\Exceptions\HttpResponseException;
use Meibuyu\Micro\Model\Auth;
use Meibuyu\Micro\Service\Interfaces\MessageServiceInterface;

class MessageHandler
{

    /**
     * @Inject()
     * @var MessageServiceInterface
     */
    private $messageService;

    /**
     * @Inject
     * @var ConfigInterface
     */
    protected $config;

    /**
     * 发送模板消息
     * @param $receiverIds
     * @param $templateId
     * @param array $replace
     * @throws HttpResponseException
     */
    public function sendTemp($receiverIds, $templateId, $replace = [])
    {
        $application = $this->config->get('app_name');
        if (!$application) {
            throw new HttpResponseException("请设置应用名app_name");
        }
        $receiverIds = is_array($receiverIds) ? $receiverIds : [$receiverIds];
        $sendUserId = Auth::id();
        $this->messageService->send($receiverIds, $application, $templateId, $sendUserId, $replace);
    }

    /**
     * 发送文本消息
     * @param $receiverIds
     * @param $content
     * @throws HttpResponseException
     */
    public function sendText($receiverIds, $content)
    {
        $application = $this->config->get('app_name');
        if (!$application) {
            throw new HttpResponseException("请设置应用名app_name");
        }
        $receiverIds = is_array($receiverIds) ? $receiverIds : [$receiverIds];
        $sendUserId = Auth::id();
        $this->messageService->send($receiverIds, $application, 0, $sendUserId, [], $content);
    }

    /**
     * 自动发送模板消息
     * @param $receiverIds
     * @param $templateId
     * @param array $replace
     * @throws HttpResponseException
     */
    public function sendAutoTemp($receiverIds, $templateId, $replace = [])
    {
        $application = $this->config->get('app_name');
        if (!$application) {
            throw new HttpResponseException("请设置应用名app_name");
        }
        $receiverIds = is_array($receiverIds) ? $receiverIds : [$receiverIds];
        $this->messageService->send($receiverIds, $application, $templateId, 0, $replace);
    }

    /**
     * 自动发送文本消息
     * @param $receiverIds
     * @param $content
     * @throws \Exception
     */
    public function sendAutoText($receiverIds, $content)
    {
        $application = $this->config->get('app_name');
        if (!$application) {
            throw new \Exception("请设置应用名app_name");
        }
        $receiverIds = is_array($receiverIds) ? $receiverIds : [$receiverIds];
        $this->messageService->send($receiverIds, $application, 0, 0, [], $content);
    }

    /**
     * 定时任务专用 发送文本消息
     * @param $receiverIds
     * @param $content
     * @throws HttpResponseException
     */
    public function sendCrontabText($receiverIds, $content)
    {
        $application = $this->config->get('app_name');
        if (!$application) {
            throw new HttpResponseException("请设置应用名app_name");
        }
        $receiverIds = is_array($receiverIds) ? $receiverIds : [$receiverIds];
        $this->messageService->send($receiverIds, $application, 0, 0, [], $content);
    }

    /**
     * 发送markdown 文本消息
     * @param $receiverIds
     * @param $content
     * @param $title
     * @throws HttpResponseException
     */
    public function sendMarkDownText($receiverIds, $content, $title)
    {
        $application = $this->config->get('app_name');
        if (!$application) {
            throw new HttpResponseException("请设置应用名app_name");
        }
        $receiverIds = is_array($receiverIds) ? $receiverIds : [$receiverIds];
        $sendUserId = Auth::id();
        $this->messageService->SendMarkDownMessage($receiverIds, $application, 0, $sendUserId, [], $content, $title);
    }

    /**
     * 自动发送markdown 文本消息
     * @param $receiverIds
     * @param $content
     * @param $title
     * @throws HttpResponseException
     */
    public function sendAutoMarkDownText($receiverIds, $content, $title)
    {
        $application = $this->config->get('app_name');
        if (!$application) {
            throw new HttpResponseException("请设置应用名app_name");
        }
        $receiverIds = is_array($receiverIds) ? $receiverIds : [$receiverIds];
        $this->messageService->SendMarkDownMessage($receiverIds, $application, 0, 0, [], $content, $title);
    }

}
