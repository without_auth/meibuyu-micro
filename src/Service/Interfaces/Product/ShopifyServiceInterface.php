<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Date: 2020/10/12
 * Time: 9:39
 */

namespace Meibuyu\Micro\Service\Interfaces\Product;

use Exception;
use Meibuyu\Micro\Exceptions\RpcException;

interface ShopifyServiceInterface
{

    /**
     * 拉取一个shopify订单数据
     * @param $orderId
     * @param $shopifySiteId
     * @return array
     * @throws Exception
     */
    public function pullOrder($orderId, $shopifySiteId): array;

    /**
     * 通过id列表获取shopify站点数组
     * @param array $ids shopify站点id数组,默认去重
     * @param array $columns shopify站点表字段,默认['name', 'prefix', 'team_id', 'site_id', 'language_id', 'url', 'currency_id']
     * @return array 默认keyBy('id')
     * @author Zero
     */
    public function getShopifySitesByIds(array $ids, $columns = ['name', 'prefix', 'team_id', 'site_id', 'language_id', 'url', 'currency_id']): array;

    /**
     * 拉取传入的id之后的shopify订单列表数据,默认50条数据
     * @param int $sinceId 订单id
     * @param int $shopifySiteId shopify站点id
     * @return array
     * @throws Exception
     * @author Zero
     */
    public function pullOrderList($sinceId, $shopifySiteId): array;

    /**
     * 更新shopify订单
     * @param int $orderId 订单id
     * @param array $params 更新的数据
     * @param int $shopifySiteId shopify站点id
     * @return mixed
     * @throws RpcException
     * @author zero
     */
    public function updateOrder($orderId, $params, $shopifySiteId);

    /**
     * 创建shopify订单发货记录
     * 参数示例: https://shopify.dev/docs/admin-api/rest/reference/shipping-and-fulfillment/fulfillment#create-2020-07
     * location_id不传会默认拿取系统有的数据,拿不到报错
     * @param int $orderId
     * @param array $params
     * @param int $shopifySiteId
     * @return mixed
     * @throws RpcException
     * @author zero
     */
    public function createOrderFulfillment($orderId, $params, $shopifySiteId);

    /**
     * 更新shopify订单发货物流信息
     * 参数示例: https://shopify.dev/docs/admin-api/rest/reference/shipping-and-fulfillment/fulfillment#update_tracking-2020-07
     * [
     *   "notify_customer" => true,
     *   "tracking_info" => [
     *     "number" => "1111",
     *     "url" => "http://www.my-url.com",
     *     "company" => "my-company",
     *    ]
     * ]
     * @param $fulfillmentId
     * @param $params
     * @param $shopifySiteId
     * @return array
     * @throws RpcException
     * @author zero
     */
    public function updateFulfillmentTracking($fulfillmentId, $params, $shopifySiteId);

    /**
     * 通过id数组获取shopify的location数据
     * @param $ids
     * @param string[] $columns
     * @return array 默认keyBy('id')
     * @author zero
     */
    public function getLocationsByIds($ids, $columns = ['*']);

}
