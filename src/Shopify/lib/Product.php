<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Date: 2020/9/2
 * Time: 16:50
 */

namespace Meibuyu\Micro\Shopify\lib;

/**
 * Class Product
 * @package Meibuyu\Micro\Shopify\lib
 *
 * @property-read ProductImage $Image
 * @property-read ProductVariant $Variant
 * @property-read Metafield $Metafield
 *
 * @method ProductImage Image(integer $id = null)
 * @method ProductVariant Variant(integer $id = null)
 * @method Metafield Metafield(integer $id = null)
 */
class Product extends AbstractShopify
{

    protected $resourceKey = 'product';

    protected $childResource = [
        'ProductImage' => 'Image',
        'ProductVariant' => 'Variant',
        'Metafield',
    ];

}
