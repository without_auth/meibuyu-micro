<?php
namespace Meibuyu\Micro\Service\Interfaces\Order;


interface StockUpServiceInterface
{
    /**
     *自动备货入库后改变备货单状态为已入库
     * @param $stockNo
     * @return mixed
     */
    public function stockIntoUpdateStatus($stockNo):bool;
}
