<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Date: 2020/9/2
 * Time: 16:50
 */

namespace Meibuyu\Micro\Shopify\lib;

/**
 * Class FulfillmentService
 * @package Meibuyu\Micro\Shopify\lib
 */
class FulfillmentService extends AbstractShopify
{

    protected $resourceKey = 'fulfillment_service';

    public $countEnabled = false;

}
