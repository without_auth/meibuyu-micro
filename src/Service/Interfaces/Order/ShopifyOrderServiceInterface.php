<?php
/**
 * Created by PhpStorm.
 * User: 姜克保
 * Date: 2020/5/20
 * Time: 15:48
 */

namespace Meibuyu\Micro\Service\Interfaces\Order;



interface ShopifyOrderServiceInterface
{
    /**
     * shopify 推送订单信息
     * @param string $topic 动作
     * @param array $array 订单信息
     * @param array $shopifySite 站点信息
     * @return mixed
     */
    public function pushOrder($topic, array $array = [], array $shopifySite = []);
}
