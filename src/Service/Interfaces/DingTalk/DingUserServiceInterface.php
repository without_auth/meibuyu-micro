<?php
/**
 * Created by PhpStorm.
 * User: 姜克保
 * Date: 2020/5/20
 * Time: 15:48
 */

namespace Meibuyu\Micro\Service\Interfaces\DingTalk;

interface DingUserServiceInterface
{
    /** 通过用户id获取单个用户信息
     * @param string $ding_user_id 钉钉用户id
     * @return array 钉钉用户信息
     */
    public function getByDingUserId($ding_user_id): array;

    /**
     * 获取部门用户列表
     * @return array
     */
    public function getDingUserLists(): array;

    /**
     * 通过临时授权码获取用户信息
     * @param string $code 临时授权码
     * @return array
     */
    public function getDingUserByTempCode($code): array;

}
