<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Date: 2020/3/23
 * Time: 17:45
 */

namespace Meibuyu\Micro\Constants;

class MaterialCategory
{

    const FU_LIAO = 1; // 辅料
    const MIAN_LIAO = 2; // 面料
    const LI_BU = 3; // 里布
    const HAO_CAI = 4; // 耗材

    const FU_LIAO_WAREHOUSE = 33;
    const MIAN_LIAO_WAREHOUSE = 36;

    /**
     * 通过原料类型获取对应仓库id
     * @param $categoryId
     * @return int
     * @throws \Exception
     */
    public static function getWarehouseId($categoryId)
    {
        switch ($categoryId) {
            case self::FU_LIAO:
            case self::HAO_CAI:
                return self::FU_LIAO_WAREHOUSE;
            case self::MIAN_LIAO:
            case self::LI_BU:
                return self::MIAN_LIAO_WAREHOUSE;
            default:
                throw new \Exception("原料类型不存在");
        }
    }

}
