# Meibuyu Library
美不语微服务官方接口库

### 1、如何使用
在使用的项目下的composer.json 加入以下内容
``` 
"repositories": {
    "meibuyu/micro": {
        "type": "path",
        "url": "path/to/micro",//本库的具体地址，随意找个地方git clone下来
        "options": {
            "symlink": true
        }
    },
}
```
然后在使用的项目下执行
``` 
composer require meibuyu/micro @dev
```

---

### 2、鉴权注解使用方法
> 使用时必须接入用户服务  
> 权限名会拼接env文件中的APP_NAME属性,请注意唯一性   
> 所有权限必须存在于用户服务的权限表中,若不存在,请联系管理员添加权限
##### 1、@AutoPerm
在控制器头部添加@AutoPerm注解,为该控制器下所有的方法添加鉴权功能,生成的权限名为`蛇形控制名_蛇形方法名`
```
/**
 * @AutoPerm()
 */
class UserInfoController {}
```
参数: 
> 1. prefix, 前缀(字符串),默认为蛇形控制名(user_info)
> 2. exclude, 要排除的方法名(字符串数组),默认为空
```
/**
 * @AutoPerm(prefix="user", exclude={"getUser"})
 */
class UserInfoController {}
```

##### 2、@Perm
在控制器中的方法头部添加@Perm注解,为当前方法添加鉴权功能,生成权限名为`蛇形控制名_蛇形方法名`
```
/**
 * @Perm()
 */
function getUser {}
```
参数: 
> name, 前缀(字符串),默认为蛇形方法名(user)
```
/**
 * @Perm("get_user")
 */
function getUser {}
```
### 3、对集合获取值、然后调用rpc方法获取数据后，重新赋值
#### 1）、获取值，设置值
```
$list = Task::with(['c'=>function($q){
    $q->select(['d','y']);
}])->select(['c','v'])->->paginate(2);
/*
* 假设拿到的数据是这个
* $list = [['c' => ['d' => 4, 'y' => 9], 'v' => '5'], ['c'=>'','v'=>'8'], ['c' => ['d' => 6, 'y' => 10], 'v' => '7']];
*/
$user_ids = get_collection_values($list, 'c.d');
// 去RPC拿用户数据
$users = $this->userService->getByIdList($hello);
//重新赋值给列表
put_collection_values($list, $users, 'c.d', 'user', 'id');
/*
* 则新的数据如下
* $list = [['c' => ['d' => 4,'user'=>['id'=>4,'name'=>'张三'], 'y' => 9], 'v' => '5'],
 ['c'=>'','v'=>'8'], 
 ['c' => ['d' => 6, ,'user'=>['id'=>6,'name'=>'王五']'y' => 10], 'v' => '7']];
*/
```
#### 2）、判断各种值和更新各种值
```
// 使用第一步获取的列表

// askModel文件 新增获取值，和设置值方法

 * 动态判断
 * @param $currentUserId
 */
public function getCanDelete($currentUserId)
{
    $this->attributes['can_delete'] = $this->user_id == $currentUserId ? true : false;
}


/**新增time_show属性
 * @return string 返回友好时间值
 */
public function getTimeShowAttribute()
{
    if ($this->status == 2) {
        return '已完成';
    }
    $time = time();
    if ($this->status === 0) {
        $time = $time - strtotime($this->start_time);
        return human_time($time) . "后开始";
    }
    if ($this->status === 1) {
        $time = $time - strtotime($this->end_time);
        if ($time > 0) {
            return "超时" . human_time($time);
        }
        return human_time($time) . "后结束";
    }
}
/**
 * 设置结束时间
 * @param string $time
 */
public function setFinishAttribute($time = '')
{
    $this->attributes['finish'] = $time ? $time : today();
}

// TaskRepositoryEloquent 文件
// 获取列表方法
.. .....
$currentUserId = Auth::id();
foreach ($list->items() as $item) {
    $item->getCanDelete($currentUserId);//结果会新增 can_delete属性
    $item->time_show;//自动调用getTimeShowAttribute方法，并设置值
     $item->finish=today();//自动调用setFinishAttribute方法，并设置值
}
return $list;//集合自动转数组，会带上新的三个属性，和分页数据
```
### 4、新方法说明
#### 1）、human_time 友好的显示时间 
用法：
```
human_time(time()-strtotime('2020-06-06 12:12'));
//12分钟等等根据计算值自动显示时分秒，不足一分钟显示秒，不足一小时显示分
//不足一天显示小时
//可以自行测试
```
#### 2）、info 输出数据到控制台，支持任何数据，自动换行 方便测试 
用法：
```
//支持多参数
info('aaa',[1,2,3],new stdClass(){$a=1;},collect([1,23,4]));
info(1);
```
