<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Date: 2020/6/18
 * Time: 8:32
 */

namespace Meibuyu\Micro\Constants;

class SpecialUserId
{

    const BOSS = 38; // 王大抗
    const ZHANG_TING_TING = 83; // 张婷婷
    const MA_BAO_TONG = 84; // 马宝同
    const CAI_YA_XIANG = 85; // 蔡亚祥
    const CAI_HONG_SHAN = 86; // 蔡红山
    const ZHAN_YONG_YONG = 87; // 詹永勇
    const LI_DONG = 292; // 李东
    const BAO_SHI_FANG= 322; // 鲍诗芳

}
