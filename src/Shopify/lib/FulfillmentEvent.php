<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Date: 2020/9/2
 * Time: 16:50
 */

namespace Meibuyu\Micro\Shopify\lib;

/**
 * Class FulfillmentEvent
 * @package Meibuyu\Micro\Shopify\lib
 */
class FulfillmentEvent extends AbstractShopify
{

    protected $resourceKey = 'event';

}
