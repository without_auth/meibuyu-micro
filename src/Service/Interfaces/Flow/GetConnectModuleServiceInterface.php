<?php


namespace Meibuyu\Micro\Service\Interfaces\Flow;


interface GetConnectModuleServiceInterface
{
    /**
     *通过通道名称获取process_id
     * @param string $channel 通道名
     * @return mixed
     */
    public function getConnectModule($channel);
}