<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Date: 2020/9/2
 * Time: 16:50
 */

namespace Meibuyu\Micro\Shopify\lib;

/**
 * Class Fulfillment
 * @package Meibuyu\Micro\Shopify\lib
 *
 * @property-read Event $Event
 *
 * @method Event Event(integer $id = null)
 *
 * @method array update_tracking($info)     Update Tracking
 * @method array complete()     Complete a fulfillment
 * @method array open()         Open a pending fulfillment
 * @method array cancel()       Cancel a fulfillment
 */
class Fulfillment extends AbstractShopify
{

    protected $resourceKey = 'fulfillment';

    protected $childResource = [
        'FulfillmentEvent' => 'Event',
    ];

    protected $customPostActions = [
        'update_tracking',
        'complete',
        'open',
        'cancel',
    ];

}
