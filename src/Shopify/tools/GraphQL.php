<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Time: 2021/1/14 9:16
 */

namespace Meibuyu\Micro\Shopify\tools;

class GraphQL
{

    public static function parseDeliveryProfileId($gid)
    {
        if ($gid && stripos($gid, 'gid://shopify/DeliveryProfile/') !== false) {
            $gid = str_replace('gid://shopify/DeliveryProfile/', '', $gid);
        }
        return (int)$gid;
    }

}
