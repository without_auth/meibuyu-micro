<?php
/**
 * Created by PhpStorm.
 * User: 王源
 * Date: 2020/1/9
 * Time: 15:07
 */

namespace Meibuyu\Micro\Service\Interfaces\Product;

interface ProductServiceInterface
{

    /**
     * 获取单个数据
     * @param int $id 产品id
     * @param array $relations 产品的关联关系，支持：["brand","category","ingredient","product_name","status","type","images","price_info","product_children"]
     * @param array $columns 产品表的字段，默认
     * ['id', 'sku', 'name', 'en_name', 'brand_id', 'team_id', 'hs_code', 'origin_country_id', 'production_address',
     * 'unit', 'category_id', 'ingredient_id', 'product_name_id', 'type_id', 'status_id', 'style', 'info_completed'
     * , 'bar_code', 'bar_code_image', 'creator_id', 'hot']
     * @return array|null
     */
    public function get($id, array $relations = [], array $columns = ['*']);

    /**
     * 通过id列表获取产品数组
     * @param array $idList 产品id的列表, 默认去重
     * @param array $relations 产品的关联关系，支持["brand","category","ingredient","product_name","status","type","images","price_info","product_children"]
     * @param array $columns 产品表的字段，默认['id', 'sku', 'name', 'en_name', 'brand_id'
     * , 'team_id', 'hs_code', 'origin_country_id', 'production_address', 'unit', 'category_id'
     * , 'ingredient_id', 'product_name_id', 'type_id', 'status_id', 'style', 'info_completed'
     * , 'bar_code', 'bar_code_image', 'creator_id', 'hot']
     * @return array 默认keyBy('id')
     */
    public function getByIdList(array $idList, array $relations = [], array $columns = ['*']): array;

    /**
     * 通过sku列表获取产品列表
     * @param array $skuList 默认去重
     * @param array $relations 产品的关联关系,可传入['color', 'size','brand', 'category', 'product_name', 'images', 'cost', 'weight']
     * @param array $columns 产品表的字段，默认返回id
     * @return array 默认keyBy('sku')
     */
    public function getListBySkuList(array $skuList, array $relations = [], array $columns = ['id']);

    /**
     * 获取全部品类列表
     * @param bool $asTree 是否作为树返回
     * @param array $columns 默认['id', 'name', 'parent_id']
     * @return array
     */
    public function categories($asTree = false, array $columns = ['id', 'name', 'parent_id']): array;

    /**
     * 获取全部品牌列表
     * @param array $columns 默认['id', 'name']
     * @return array
     */
    public function brands(array $columns = ['id', 'name']): array;

    /**
     * 获取全部报关品名列表
     * @param array $columns 默认['id', 'name','en_name']
     * @return array
     */
    public function productNames(array $columns = ['id', 'name', 'en_name']): array;

    /**
     * 获取全部成分列表
     * @param array $columns 默认['id', 'name']
     * @return array
     */
    public function ingredients(array $columns = ['id', 'name', 'en_name']): array;

    /**
     * 通过id数组获取品类列表
     * @param array $ids 默认去重
     * @param array $columns
     * @return array 默认keyBY('id')
     */
    public function getCategoriesByIds(array $ids, array $columns = ['id', 'name', 'parent_id']): array;

    /**
     * 通过id数组获取品名列表
     * @param array $ids 默认去重
     * @param array $columns
     * @param array $relations ['report_points']
     * @return array 默认keyBY('id')
     */
    public function getProductNamesByIds(array $ids, array $columns = ['id', 'name', 'en_name'], array $relations = []): array;

    /**
     * 通过id获取品类
     * @param int $id
     * @param array $columns
     * @return array|null
     */
    public function getCategoryById(int $id, array $columns = ['id', 'name', 'parent_id']);

    /**
     * 通过id获取品名
     * @param int $id
     * @param array $columns
     * @param array $relations ['report_points']
     * @return array|null
     */
    public function getProductNameById(int $id, array $columns = ['id', 'name', 'en_name'], array $relations = []);

    /**
     * 通过id数组获取申报要素列表
     * @param array $ids 默认去重
     * @param array $columns
     * @param array $relations ['product_name', 'ingredient']
     * @return array 默认keyBY('id')
     */
    public function getReportPointsByIds(array $ids, array $relations = [], array $columns = ['id', 'hs_code', 'point']): array;

    /**
     * 通过id获取申报要素
     * @param int $id
     * @param array $columns
     * @param array $relations ['product_name', 'ingredient']
     * @return array|null
     */
    public function getReportPointById(int $id, array $relations = [], array $columns = ['id', 'hs_code', 'point']);

    /**
     * 获取申报要素数据
     * @param array $idList 默认去重
     * @param bool $groupByFlag
     * @return array keyBy('id')
     */
    public function getWithPoint(array $idList, $groupByFlag = false);

    /**
     * 获取全部申报要素列表
     * @param array $columns 默认['*']
     * @return array
     */
    public function reportPoints(array $columns = ['*']): array;

    /**
     * 通过产品id获取维护的所有尺码
     * @param $id
     * @return array
     * @author Zero
     */
    public function getSizesById($id): array;

    /**
     * 完成产品审批
     * @param $data
     * @author Zero
     */
    public function downApprove($data);

}
