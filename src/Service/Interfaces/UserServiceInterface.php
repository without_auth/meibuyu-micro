<?php
/**
 * Created by PhpStorm.
 * User: 王源
 * Date: 2020/1/9
 * Time: 15:07
 */

namespace Meibuyu\Micro\Service\Interfaces;

interface UserServiceInterface
{

    /**
     * 通过用户名称模糊获取用户
     * @param string $name
     * @param array $columns
     * @return array
     */
    public function getByName(string $name, array $columns = ['id', 'name']): array;

    /**
     * 获取单个数据
     * @param int $id
     * @param array $columns
     * @param array $relations 可传入['teams', 'departments', 'position', 'assessment_plan'],分别是团队,部门,岗位和考核方案
     * @return mixed
     */
    public function get(int $id, array $columns = ['*'], array $relations = []);

    /**
     * 获取全部数据
     * @param array $columns 默认['id', 'name']
     * @param array $relations 可传入['teams', 'departments', 'position', 'assessment_plan'],分别是团队,部门,岗位和考核方案
     * @return array
     */
    public function all(array $columns = ['id', 'name'], array $relations = []): array;

    /**
     * 通过id列表获取用户数组
     * @param array $idList 默认去重
     * @param array $columns
     * @param array $relations 可传入['teams', 'departments', 'position', 'assessment_plan'],分别是团队,部门,岗位和考核方案
     * @return mixed 默认keyBY('id')
     */
    public function getByIdList($idList, $columns = ['*'], $relations = []);

    /**
     * 通过部门id列表获取用户数组(包括子部门用户)
     * @param array $deptIds 默认去重
     * @param array $columns 用户字段,默认['id', 'name']
     * @return array
     */
    public function getListByDeptIds(array $deptIds, $columns = []);

    /**
     * 通过团队id数组获取用户数组(包括子团队用户)
     * @param array $teamIds 默认去重
     * @param array $columns 用户字段,默认['id', 'name']
     * @return array
     */
    public function getListByTeamIds(array $teamIds, $columns = []);

    /**
     * 判断是否是超级管理员
     * @param int $userId
     * @return bool
     */
    public function isSuperAdmin(int $userId): bool;

    /**
     * 鉴权
     * @param int $userId
     * @param string $perm
     * @return bool
     */
    public function checkPerm(int $userId, string $perm): bool;


    /**
     * 获取当前数据权限的配置
     * @example store_warehouse_index(store_模块的名称，warehouse_ 控制器 index 方法)
     * @param string $dataPerm
     * @return array
     */
    public function checkDataPerm(string $dataPerm): array;

    /**
     * 获取用户拥有某个应用的所有权限
     * @param int $userId
     * @param mixed $appNames 应用名,多个传数组
     * @return array
     */
    public function getPerms(int $userId, $appNames = null): array;

    /**
     * 获取带领导字符串数组的列表
     * [
     *     ['id' => -1, 'name' => '总经理'],
     *     ['id' => -2, 'name' => '直属领导'],
     *     ['id' => -3, 'name' => '部门领导'],
     *     [...]
     * ]
     * @return array
     */
    public function allWithLeader(): array;

    /**
     * 获取带有领导真实信息的用户列表
     * @param int $userId 默认去重
     * @param array $idList 可包含[-1,-2,-3]
     * @param array $columns
     * @return array 默认keyBY('id')
     */
    public function getListWithLeader(int $userId, array $idList = [], array $columns = ['id', 'name']): array;

    /**
     * 更新用户考核方案
     * @param $userId
     * @param array $attributes
     * 'review_users' => '1,2,3' // 点评人
     * 'cc_person' => '1,2,3' // 抄送人
     * @return int
     */
    public function updateAssessmentPlan($userId, array $attributes);

    /**
     * 获取用户直属领导
     * @param int $userId 用户id
     * @return array
     */
    public function getDirectLeader($userId);

    /**
     * 获取多个用户的直属领导
     * @param array $userIds 用户id数组,默认去重
     * @return array 直属领导数组,key值为用户id
     */
    public function getDirectLeadersByIds($userIds);

    /**
     * 判断用户是否属于某个部门
     * @param mixed $userId 用户id
     * @param mixed $deptId 部门id,判断多个部门传数组
     * @param bool $withChildren 是否包含子部门,默认false
     * @return bool
     * @author Zero
     */
    public function belongToDepartments($userId, $deptId, $withChildren = false): bool;

}
