<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Date: 2020/8/19
 * Time: 9:25
 */

namespace Meibuyu\Micro\Shopify;

use Hyperf\Contract\ContainerInterface;

class ShopifyFactory
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function create(array $config = []): ShopifyApp
    {
        if (method_exists($this->container, 'make')) {
            // Create by DI for AOP.
            return $this->container->make(ShopifyApp::class, ['config' => $config]);
        }
        return new ShopifyApp($config);
    }

}
