<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Date: 2020/8/24
 * Time: 16:50
 */

namespace Meibuyu\Micro\Shopify\lib;

/**\
 * Class SmartCollection
 * @package Meibuyu\Micro\Shopify\lib
 *
 * @property-read Metafield $Metafield
 *
 * @method Metafield Metafield(integer $id = null)
 */
class SmartCollection extends AbstractShopify
{

    protected $resourceKey = 'smart_collection';

    protected $childResource = [
        'Metafield',
    ];

}
