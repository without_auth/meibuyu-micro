<?php

namespace Meibuyu\Micro\Shopify\tools;

use Exception;

class CurlRequest
{

    protected static function init($url, $httpHeaders = [])
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'PHPClassic/PHPShopify');
        $headers = [];
        foreach ($httpHeaders as $key => $value) {
            $headers[] = "$key: $value";
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        return $ch;
    }

    /**
     * @param $url
     * @param array $httpHeaders
     * @return array
     * @throws Exception
     * @author zero
     */
    public static function get($url, $httpHeaders = [])
    {
        $ch = self::init($url, $httpHeaders);
        return self::processRequest($ch);
    }

    /**
     * @param $url
     * @param $data
     * @param array $httpHeaders
     * @return array
     * @throws Exception
     * @author zero
     */
    public static function post($url, $data, $httpHeaders = [])
    {
        $ch = self::init($url, $httpHeaders);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        return self::processRequest($ch);
    }

    /**
     * @param $url
     * @param $data
     * @param array $httpHeaders
     * @return array
     * @throws Exception
     * @author zero
     */
    public static function put($url, $data, $httpHeaders = [])
    {
        $ch = self::init($url, $httpHeaders);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        return self::processRequest($ch);
    }

    /**
     * @param $url
     * @param array $httpHeaders
     * @return array
     * @throws Exception
     * @author zero
     */
    public static function delete($url, $httpHeaders = [])
    {
        $ch = self::init($url, $httpHeaders);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        return self::processRequest($ch);
    }

    /**
     * @param $ch
     * @return array
     * @throws Exception
     * @author zero
     */
    protected static function processRequest($ch)
    {
        $output = curl_exec($ch);
        $response = new CurlResponse($output);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
//        if ($httpCode == 429) {
//            $limitHeader = explode('/', $response->getHeader('X-Shopify-Shop-Api-Call-Limit')[0], 2);
//            if (isset($limitHeader[1]) && $limitHeader[0] < $limitHeader[1]) {
//                throw new Exception($response->getBody());
//            }
//        }
        if (curl_errno($ch)) {
            throw new Exception(curl_errno($ch) . ' : ' . curl_error($ch));
        }
        curl_close($ch);
        return [$httpCode, $response->getHeaders(), $response->getBody()];
    }

}
