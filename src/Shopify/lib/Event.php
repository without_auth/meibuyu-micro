<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Date: 2020/9/2
 * Time: 16:50
 */

namespace Meibuyu\Micro\Shopify\lib;

/**
 * Class Event
 * @package Meibuyu\Micro\Shopify\lib
 */
class Event extends AbstractShopify
{

    protected $resourceKey = 'event';

}
