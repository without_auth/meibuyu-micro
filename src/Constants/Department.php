<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Time: 2020/10/27 9:21
 */

namespace Meibuyu\Micro\Constants;

class Department
{

    const WAREHOUSE_LOGISTICS = 5; // 仓储物流部
    const WAREHOUSE = 6; // 仓储部
    const LOGISTICS = 7; // 物流部
    const FINANCE = 10; //财务部
    const HR = 18; // 行政人事部
    const TECH = 34; // 技术部

}
