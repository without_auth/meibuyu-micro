<?php
/**
 * Created by PhpStorm.
 * User: Zero
 * Date: 2020/9/2
 * Time: 16:50
 */

namespace Meibuyu\Micro\Shopify\lib;

/**
 * Class Order
 * @package Meibuyu\Micro\Shopify\lib
 *
 * @property-read Fulfillment $Fulfillment
 * @property-read Event $Event
 *
 * @method Fulfillment Fulfillment(integer $id = null)
 * @method FulfillmentOrder FulfillmentOrder()
 * @method Event Event(integer $id = null)
 *
 * @method array close()     Close an Order
 * @method array open()         Re-open a closed Order
 * @method array cancel(array $data)  Cancel an Order
 */
class Order extends AbstractShopify
{

    protected $resourceKey = 'order';

    protected $childResource = [
        'Fulfillment',
        'FulfillmentOrder',
        'Event',
    ];

    protected $customPostActions = [
        'close',
        'open',
        'cancel',
    ];

}
