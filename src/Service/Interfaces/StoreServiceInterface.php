<?php
/**
 * Created by PhpStorm.
 * User: 王源
 * Date: 2020/3/16
 * Time: 15:07
 */

namespace Meibuyu\Micro\Service\Interfaces;

interface StoreServiceInterface
{
    /**
     * 通过id列表获取仓库名称
     * @param array $idList 仓库id的列表, 默认去重
     * @param array $columns 仓库表的字段，默认显示全部
     * @return array 默认keyBy('id')
     */
    public function getByIdList(array $idList, array $columns = ['*']): array;

    /**
     * description:通过产品id数组获取库存列表 给订单系统查询的接口
     * author: fuyunnan
     * @param array $ids 产品ids 数组
     * @param int $wareId 仓库id
     * @param array $notWareId 仓库id
     * @return array
     * @throws
     * Date: 2020/7/27
     */
    public function getListStock($ids, $wareId = 0, $notWareId = []): array;

    /**
     * description:根据筛选条件获取仓库的库存列表 对库存筛选实现大一统
     * 参数格式 不需要可不传入对应的键
     *  $args['inTeamIds'] =[];
    $args['notInTeamIds'] = [];
    $args['inWhIds'] = []; //仓库id
    $args['notInWhIds'] = [];
    $args['inProductIds'] = [];
    $args['notInProductIds'] = [];
    $args['where'] = [];
    $args['group'] = ['product_id'];//默认['product_id']
    $args['keyBy'] = 'product_id';//默认排序
    $args['isKeyBy'] = $condition['isKeyBy'] ?? true;//是否加keyBy;
    $args['isGroupBy'] = $condition['isGroupBy'] ?? true;//是否加分组;
     *
     * author: fuyunnan
     * @param array $condition 筛选条件
     * @return array
     * @throws
     * Date: 2020/7/27
     */
    public function getListStockWhere($condition): array;

    /**
     * description:检查是否有库存，有就返回库存数量（有记录） 给产品系统使用
     * author: fuyunnan
     * @param array $ids 仓库产品的id数组
     * @return array
     * @throws
     * Date: 2020/7/31
     */
    public function checkStock($ids): array;

    /**
     * description:创建入库单
     * author: fuyunnan
     * @param array $data 需要入库的数组 格式 入库添加
     * *
     * data[master][warehousing_date]:2020-01-08 入库时间
     * data[master][creator_id]:12 创建人id
     * data[master][warehouse_id]:2 仓库id
     * data[master][type_id]:1 入库单类型
     * data[master][source_no]:no_121333 来源单号 （选填）
     * data[master][stock_up_status]:1二次质检状态 （选填）
     * data[master][remark]:备注 （选填）
     * data[master][status]:2 状态 （选填 不填默认1）
     *
     * 产品二维数组
     * data[goods][0][product_id]:16 产品id
     * data[goods][0][should_cnt]:133 应入数量
     * data[goods][0][real_cnt]:10 实入数量
     * data[goods][1][product_id]:18
     * data[goods][1][should_cnt]:10
     * data[goods][1][real_cnt]:15
     *
     * 或者参考yapi  http://api.huaperfect.com/project/38/interface/api/1617
     *
     * @return array
     * @throws
     * Date: 2020/7/6
     */
    public function createWarehousing(array $data): array;

    /**
     * description:批量创建出库单
     * author: fuyunnan
     * [
     * 'main' => [
     *     'creator_id' => 1,
     *     'warehouse_id' => 1,
     *     'type' => 5,
     * ],
     * 'products' => [
     * [
     *     'source_no' => 1,
     *     'product_id' => 1,
     *     'real_cnt' => 1
     * ],
     * [
     *     'source_no' => 1,
     *     'product_id' => 1,
     *     'real_cnt' => 1
     * ]
     * ]
     * ]
     *
     * @param array $attributes 提交的数组  或参考yapi http://api.huaperfect.com/project/38/interface/api/6968
     * @return bool
     * @throws
     * Date: 2020/8/8
     */
    public function createBatchWarehousing(array $attributes): bool;


    /**
     * description:根据入库单状态，判断是否生成出库单
     * author: fuyunnan
     * @param array $data 需要入库或者出库的数据
     * @return bool
     * @throws
     * Date: 2021/5/25
     */
    public function createInOrOutOrder(array $data): bool;


    /**
     * description:创建入库单 支持自定义 入库单状态和仓位
     * author: fuyunnan
     * @param array $attributes 需要入库的数组 格式请参考yapi 入库添加
     * @return bool
     * @throws
     * Date: 2020/7/6
     */
    public function createWarehousingStatus(array $attributes): bool;


    /**
     * description:批量创建出库单,多个仓库 直接出虚拟仓
     * author: fuyunnan
     * @param array $attributes 提交的数组  参考yapi http://api.huaperfect.com/project/38/interface/api/6968
     * @return bool
     * @throws
     * Date: 2020/8/8
     */
    public function createBatchWarehousingStatus(array $attributes): bool;



    /**
     * description:批量创建出库单,出库单直接出库
     * author: fuyunnan
     * @param array $attributes 提交的数组  参考yapi http://api.huaperfect.com/project/38/interface/api/6968
     * @return bool
     * @throws
     * Date: 2020/8/8
     */
    public function createBatchExComplete(array $attributes): bool;

    /**
     * description:批量创建入库单
     * author: fuyunnan
     * @param array $attributes 需要入库的数组 格式请参考yapi 入库添加
     * @return bool
     * @throws
     * Date: 2020/7/6
     */
    public function createBatchInOrder(array $attributes): bool;


    /**
     * description:批量创建入库单,能自定义入库单状态
     * author: fuyunnan
     * @param array $attributes 需要入库的数组 格式请参考yapi 入库添加
     * @return bool
     * @throws
     * Date: 2020/7/6
     */
    public function createBatchInOrderStatus(array $attributes): bool;

    /**
     * description:生产单结束获取统计单号值
     * author: fuyunnan
     * @param string $sourceNo 生产单号
     * @return array
     * @throws
     * Date: 2020/7/10
     */
    public function CntSourceNoOrder($sourceNo): array;

    /**
     * description:通过来源单号获取产品列表
     * author: fuyunnan
     * @param string $sourceNo 生产单号
     * @return array
     * @throws
     * Date: 2020/7/10
     */
    public function listSourceNoProduct($sourceNo): array;

    /**
     * description:根据来源单号，获取分组后的入库单
     * author: fuyunnan
     * @param array $sourceNoList 单号列表
     * @param array $where 刷选列表
     * @return array
     * @throws
     * Date: 2020/7/11
     */
    public function listGroupOrder($sourceNoList, $where = []): array;

    /**
     * description:根据条件查询入库单信息
     * author: fuyunnan
     *  参数注意 必须这样传!!!
     * $condition = [
     * 'warehousing_order_ids' =>$ids 可选  入库单ids数组
     * ]
     * @param array $condition 条件
     * @return array
     * @throws
     * Date: 2020/7/11
     */
    public function getListInOrder($condition): array;

    /**
     * description:通过入库单id数组获取产品列表
     * author: fuyunnan
     * @param array $ids 入库单ids 数组
     * @return array
     * @throws
     * Date: 2020/7/10
     */
    public function listIdsProduct($ids): array;

    /**
     * description:修改二次质检状态
     * author: fuyunnan
     * @param int $orderId 入库单id
     * @param array $update 修改数组
     * @return int
     * @throws
     * Date: 2020/7/14
     */
    public function updateQualityStatus($orderId, $update): int;

    /**
     * description:修改二次质检状态
     * author: fuyunnan
     * @param array $orderIds 入库单id数组
     * @param array $update 修改数组
     * @return int
     * @throws
     * Date: 2020/7/14
     */
    public function updateListQualityStatus($orderIds, $update): int;

    /**
     * description:出库单 恢复库存 出库单定为已取消
     * author: fuyunnan
     * @param array $sourceNo 来源单号数组
     * @return int
     * @throws
     * Date: 2020/9/7
     */
    public function restoreStock($sourceNo): int;

    /**
     * description:通过出库来源单号获取-对应的信息
     * author: fuyunnan
     * @param array $sources 来源单号数组
     * @param array $status 出库单状态
     * @param array $where 查询条件数组
     * @return array
     * @throws
     * Date: 2020/11/5
     */
    public function getByExSourceList($sources, $status = [], $where = []): array;

    /**
     * description:自动返回匹配的符合当前sku
     * author: fuyunnan
     * @param array $data sku数组
     * @return array
     * @throws
     * Date: 2020/11/25
     */
    public function autoSkuStock($data): array;
    /**
     * description:订单通知取消更改字段 不在生成出库单
     * author: fuyunnan
     * @param string $sourceNos 来源单号
     * @param array $field 修改字段
     * @return int
     * @throws
     * Date: 2020/12/14
     */
    public function cancelOrderDeliver($sourceNos, $field): int;
}
