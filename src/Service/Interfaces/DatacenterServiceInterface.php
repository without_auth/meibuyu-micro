<?php
/**
 * Created by PhpStorm.
 * User: 王源
 * Date: 2020/3/16
 * Time: 15:07
 */

namespace Meibuyu\Micro\Service\Interfaces;

interface DatacenterServiceInterface
{

    /**
     * description:获取国家汇率 默认取最新一个汇率值
     * author: fuyunnan
     * @param int $currencyId 货币id
     * @param array $field 字段
     * @param array $where 筛选数组
     * @return array
     * @throws
     * Date: 2020/8/11
     */
    public function getCurrencyRate($currencyId, $field = ['id', 'currency_id', 'rate_val'], $where = []): array;


    /**
     * description:获取国家汇率 默认取最新一个汇率值
     * author: fuyunnan
     * @param array $currencyIds 货币ids
     * @param array $field 字段
     * @param array $where 筛选数组
     * @return array
     * @throws
     * Date: 2020/8/11
     */
    public function getCurrencyListRate($currencyIds, $field = ['id', 'currency_id', 'rate_val'], $where = []): array;


}